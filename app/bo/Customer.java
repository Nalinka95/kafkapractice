package bo;

import java.util.HashMap;
import java.util.List;

public class Customer {
    private int customerId;
    private String name;
    private HashMap<String,String> address;
    private String addressLineOne;
    private String addressLineTwo;
    private String streetNo;
    private String landmark;
    private String city;
    private String country;
    private String zipCode;
    private String age;
    private boolean activeStatus;

    public Customer(int customerId, String name, String addressLineOne, String addressLineTwo, String streetNo, String landmark, String city, String country, String zipCode, String age, boolean activeStatus) {

        setCustomerId(customerId);
        setName(name);
        setAddressLineOne(addressLineOne);
        setAddressLineTwo(addressLineTwo);
        setStreetNo(streetNo);
        setLandmark(landmark);
        setCity(city);
        setCountry(country);
        setZipCode(zipCode);
        setAge(age);
        setActiveStatus(activeStatus);

        address.put("addressLineOne",getAddressLineOne());
        address.put("addressLineTwo",getAddressLineTwo());
        address.put("streetNo",getStreetNo());
        address.put("landmark",getLandmark());
        address.put("city",getCity());
        address.put("country",getCountry());
        address.put("zipCode",getZipCode());
    }

    public Customer(int customerId, String name,HashMap<String,String> address,String age, boolean activeStatus) {
        setCustomerId(customerId);
        setName(name);
        setAddress(address);
        setAge(age);
        setActiveStatus(activeStatus);
    }


    public int getCustomerId() {
        return customerId;
    }

    public void setAddress(HashMap<String, String> address) {
        this.address = address;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public HashMap<String, String> getAddress() {
        return address;
    }


    public String getAddressLineOne() {
        return addressLineOne;
    }

    private void setAddressLineOne(String addressLineOne) {
        this.addressLineOne = addressLineOne;
    }

    public String getAddressLineTwo() {
        return addressLineTwo;
    }

    private void setAddressLineTwo(String addressLineTwo) {
        this.addressLineTwo = addressLineTwo;
    }

    public String getStreetNo() {
        return streetNo;
    }

    private void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getLandmark() {
        return landmark;
    }

    private void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getCity() {
        return city;
    }

    private void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    private void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    private void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAge() {
        return age;
    }

    private void setAge(String age) {
        this.age = age;
    }

    public boolean isActiveStatus() {
        return activeStatus;
    }

    private void setActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }
}
