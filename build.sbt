name := """Production"""
organization := "pc"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  guice,
  "org.pac4j" % "pac4j-mongo" % "3.8.3",
  "org.mongodb" % "mongo-java-driver" % "3.8.2",
  "log4j" % "log4j" % "1.2.17",
  "org.apache.kafka" % "kafka-clients" % "1.0.0"

)
